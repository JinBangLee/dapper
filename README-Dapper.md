# Dapper总结

### 时间
2019年06月14日
## 介绍
是一款轻量级的国外开源的ORM(Object Relation Mapping)工具

## 优点：
##### 国外开源,持续更新
##### 查询效率高(仅次于IDataReader)
##### 相较于EF、NHibernate更轻量，安装后只有一个SqlMapper.cs类文件(125k)
##### 支持多种数据库sqllite,oracle,mysql,sql server
##### 兼容Framework多个版本 2.0~4.5

## GigHub地址：
https://github.com/StackExchange/Dapper

## 使用方法

### 1.要用到Dapper的项目安装 Dapper的Nuget包
### 2.创建DapperHelper类文件，用来和数据库交互
#### 需引入dapper的命名空间 using Dapper;
### 3.使用dapper执行数据的Insert ,Delete,Update,Select操作
#### A Insert,Update,Delete 语法 
'''c#
        public bool Insert(Student student)
        {

            using (IDbConnection db = new SqlConnection(SQLDBHelper.ConnctionString))
            {
                string sql ="insert into Student(FirstName,LastName,CreateTime)  VALUES (@FirstName,@LastName,@CreateTime)";
                int result = db.Execute(sql, student);
                return result > 0;
            }
        }
'''
'''c# 
public bool MultiInsert(List<Student> stuList)
        {

            using (IDbConnection db = new SqlConnection(SQLDBHelper.ConnctionString))
            {
                string sql =
                    "insert into Student(FirstName,LastName,CreateTime)  VALUES (@FirstName,@LastName,@CreateTime)";
                int result = db.Execute(sql, stuList);
                return result > 0;
            }
        }
'''
#### B Query 语法
'''c# 
public List<Student> GetStudents()
        {

            var stuList = new List<Student>();
            using (IDbConnection db = new SqlConnection(SQLDBHelper.ConnctionString))
            {
                string sql = "select * from Student";
                stuList = db.Query<Student>(sql,null).ToList();
            }
            return stuList;
        }
'''
#### C Dapper中事务操作
'''c# 
public int InsertWithTransaction(Student student)
        {

            int result = 0;
            using (IDbConnection db = new SqlConnection(SQLDBHelper.ConnctionString))
            {
                db.Open();
                IDbTransaction transaction = db.BeginTransaction();
                try
                {
                    string sql =
                    "insert into Student(FirstName,LastName,CreateTime)  VALUES (@FirstName,@LastName,@CreateTime)";
                    result = db.Execute(sql, student,transaction);
                    transaction.Commit();
                }catch(Exception ex)
                {
                    transaction.Rollback();
                }
            }
            return result;
        }
'''
